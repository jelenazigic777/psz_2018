class Task2_queries:

    COUNT_RECORD_GENRES = """SELECT genres.name, COUNT(*) FROM genres JOIN recordgenres ON genres.id=recordgenres.id_genre GROUP BY genres.name"""

    COUNT_RECORD_STYLES = """SELECT styles.name, COUNT(*) FROM styles JOIN recordstyles ON styles.id=recordstyles.id_style GROUP BY styles.name"""

    NUM_VERSION_FIRST_10 = """SELECT records.title, records.num_version
                        FROM records INNER JOIN (SELECT DISTINCT records.num_version FROM records JOIN recordauthors ON records.id=recordauthors.id_record
                        JOIN recordformats ON records.id=recordformats.id_record
                        WHERE records.num_version >= 0 AND recordformats.format_name='Album'
                        ORDER BY records.num_version DESC LIMIT 10) AS smt ON records.num_version=smt.num_version ORDER BY records.num_version DESC""" 

    PERSONS_FIRST_50_CREDITS = """SELECT persons.name, persons.credits FROM persons ORDER BY persons.credits DESC LIMIT 50"""

    PERSONS_FIRST_50_VOCALS = """SELECT persons.name, persons.vocals FROM persons ORDER BY persons.vocals DESC LIMIT 50"""

    PERSONS_FIRST_50_WRAR = """SELECT persons.name, persons.wr_and_ar FROM persons ORDER BY persons.wr_and_ar DESC LIMIT 50"""

    TRACKS_FIRST_100 = """SELECT tracks.name, tracks.num_albums, tracks.id FROM tracks 
                          JOIN trackalbums ON tracks.id=trackalbums.id_track
                          JOIN records ON records.id= trackalbums.id_album
                          JOIN recordformats ON records.id=recordformats.id_record
                          WHERE recordformats.format_name='Album'
                          GROUP BY tracks.name
                          ORDER BY tracks.num_albums DESC LIMIT 100"""

    FIND_ALBUMS_OF_TRACK_BY_ID ="""SELECT id_album FROM trackalbums 
                                    WHERE id_track=(%s)"""

    #CHANGE
    FIND_ALBUMS_BY_ID = """SELECT *
                           FROM records 
                           WHERE records.id=(%s) """

    FIND_AUTHORALBUM_NAME_BY_ID = """SELECT author_name FROM recordauthors WHERE id_record=(%s)"""

    FIND_FORMATALBUM_NAME_BY_ID = """SELECT format_name FROM recordformats WHERE id_record=(%s)"""

    FIND_GENREALBUM_NAME_BY_ID = """SELECT id_genre FROM recordgenres WHERE id_record=(%s)"""

    FIND_STYLEALBUM_NAME_BY_ID = """SELECT id_style FROM recordstyles WHERE id_record=(%s)"""

    FIND_GENRE_NAME_BY_ID = """SELECT name FROM genres WHERE id=(%s)"""

    FIND_STYLE_NAME_BY_ID = """SELECT name FROM styles WHERE id=(%s)"""


                           

