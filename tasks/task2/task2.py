# -*- coding: utf-8 -*-
import mysql.connector
from task2_queries import Task2_queries

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="stampac",
    database="PSZ_project_DB"
    )

def do_task2():
    f = open("task2.txt", "w")

    write_to_file(f, Task2_queries.COUNT_RECORD_GENRES, '2.a) GENRES')
    write_to_file(f, Task2_queries.COUNT_RECORD_STYLES, '2.b) STYLES')
    f.write('\n')
    write_to_file(f, Task2_queries.NUM_VERSION_FIRST_10, '2.c) FIRST 10 ALBUMS')
    f.write('\n')
    f.write('2.d) FIRST 50 PERSONS:' + '\n\n')
    write_to_file(f, Task2_queries.PERSONS_FIRST_50_CREDITS, 'Credits')
    write_to_file(f, Task2_queries.PERSONS_FIRST_50_VOCALS, 'Vocals')
    write_to_file(f ,Task2_queries.PERSONS_FIRST_50_WRAR, 'Writing & Arrangement')
    write_100_tracks(f)
    f.write('\n')  
    f.close()

def write_to_file(f, query, text):
    mycursor = mydb.cursor()
    mycursor.execute(query)
    data = mycursor.fetchall()
    f.write('\n')
    f.write(text + ':' + '\n\n')
    for d in data:
        f.write(str(d) + '\n')

def write_100_tracks(f):
    f.write('2.e) FIRST 100 TRACKS:' + '\n\n')
    mycursor = mydb.cursor()
    mycursor.execute(Task2_queries.TRACKS_FIRST_100)
    data = mycursor.fetchall()
    i = 1

    for d in data:
        f.write(str(i)+ '. ')
        i = i+1
        f.write('Track name: ' + str(d[0].encode('utf-8')) + '\n')
        f.write('Number of albums: ' + str(d[1]) + '\n')
        #now find al albums IDS by tracks IDS
        data = (d[2],)
        mycursor.execute(Task2_queries.FIND_ALBUMS_OF_TRACK_BY_ID, data)
        album_ids = mycursor.fetchall()
        #get album data
        f.write('Albums data: \n\n')
        for album in album_ids:
            record_id = (album[0],)
            mycursor.execute(Task2_queries.FIND_ALBUMS_BY_ID, record_id)
            album_data = mycursor.fetchall()
            album_data = album_data[0]
            f.write('Name: ' + str(album_data[1].encode('utf-8')) + '\n')
            id_album_data = (album_data[0],)

            mycursor.execute(Task2_queries.FIND_AUTHORALBUM_NAME_BY_ID, id_album_data)
            authors_names = mycursor.fetchall()

            mycursor.execute(Task2_queries.FIND_FORMATALBUM_NAME_BY_ID, id_album_data)
            authors_formats = mycursor.fetchall()

            mycursor.execute(Task2_queries.FIND_GENREALBUM_NAME_BY_ID, id_album_data)
            genre_ids = mycursor.fetchall()

            mycursor.execute(Task2_queries.FIND_STYLEALBUM_NAME_BY_ID, id_album_data)
            style_ids = mycursor.fetchall()

            f.write('Authors: \n')
            for name in authors_names:
                f.write(str(name[0].encode('utf-8')) + '\n')
            
            f.write('Formats: \n')
            for formatt in authors_formats:
                f.write(str(formatt[0]) + '\n')

            f.write('Country: ' + str(album_data[2]) + '\n')
            f.write('Year: ' + str(album_data[3]) + '\n')

            f.write('Genres: \n')
            for idd in genre_ids:
                mycursor.execute(Task2_queries.FIND_GENRE_NAME_BY_ID,idd)
                genre_name = mycursor.fetchall()
                f.write(str(genre_name[0])  + '\n')
            
            f.write('Styles: \n')
            for idd in style_ids:
                mycursor.execute(Task2_queries.FIND_STYLE_NAME_BY_ID,idd)
                style_name = mycursor.fetchall()
                f.write(str(style_name[0])  + '\n')

            f.write('\n\n')
 
do_task2()
    