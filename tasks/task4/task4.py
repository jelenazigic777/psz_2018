from argparse import ArgumentParser
import sys
import mysql.connector
from task4_queries import Task4_queries

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import Normalizer, StandardScaler
from sklearn.cluster import KMeans
import numpy as np

import matplotlib.pyplot as plt

from sklearn.decomposition import PCA

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="stampac",
    database="PSZ_project_DB"
    )

def init_bag_of_words():
    bag_of_words = {}
    mycursor = mydb.cursor()
    mycursor.execute(Task4_queries.GET_ALL_RECORDS_IDS)
    data = mycursor.fetchall()
    for d in data:
        bag_of_words[d[0]] = { 'bag_of_words_features': []}
    return bag_of_words

def get_all_records_years_and_versions():
    year_list, version_list = [], []
    mycursor = mydb.cursor()
    mycursor.execute(Task4_queries.GET_ALL_RECORDS_YEARS_VERSIONS)
    data = mycursor.fetchall()
    for d in data:
        year_list.append(d[0])
        version_list.append(d[1])
    return year_list, version_list

def draw_kmeans(data, args):

    #write data in file - first 100 lines
    i = 0
    mat = np.matrix(data)
    with open('task4.txt','wb') as f:
        for line in mat:
            np.savetxt(f, line, fmt='%.2f')
            i = i + 1
            if (i == 100):
                break

    kmeans = KMeans(n_clusters=args.k).fit(data)
    labels = kmeans.labels_
    centers = kmeans.cluster_centers_

    #dimension reduction via PCA
    pca = PCA(n_components=2).fit(data)
    pca_2d = pca.transform(data)

    plt.figure('K-means with ' + str(args.k) + ' clusters')
    plt.scatter(pca_2d[:, 0], pca_2d[:, 1], c=labels)
    plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)
    plt.show() 

def main(args):
    year_list, version_list = get_all_records_years_and_versions()

    mycursor = mydb.cursor()
    mycursor.execute(Task4_queries.GET_RECORD_COUNT)
    x = mycursor.fetchall()[0][0]
    y = 0
    data_alg = np.zeros((x, args.year + args.version))

    if args.year:
        year_list = np.reshape(year_list, (-1, 1))
        year_list_scaled = StandardScaler().fit_transform(year_list.astype(float))
        data_alg[:,y] = year_list_scaled[:, 0]
        #move to next column
        y = y + 1
    
    if args.version:
        version_list = np.reshape(version_list, (-1, 1))
        version_list_scaled = StandardScaler().fit_transform(version_list.astype(float))
        data_alg[:, y] = version_list_scaled[:, 0]
        y = y + 1

    #if bag of words
    if (args.genre or args.style):

        bag_of_words = init_bag_of_words()

        if args.genre:
            mycursor = mydb.cursor()
            mycursor.execute(Task4_queries.GET_ALL_RECORDS_GENRES)
            data = mycursor.fetchall()
            for d in data:
                bag_of_words[d[0]]['bag_of_words_features'].append(d[1])

        if args.style:
            mycursor = mydb.cursor()
            mycursor.execute(Task4_queries.GET_ALL_RECORDS_STYLES)
            data = mycursor.fetchall()
            for d in data:
                bag_of_words[d[0]]['bag_of_words_features'].append(d[1])       

        #convert to string
        for k in bag_of_words:
            bag_of_words[k]['bag_of_words_features'] = " ".join(bag_of_words[k]['bag_of_words_features'])

        #convert to list now - keys out not needed
        bag_of_words = list(bag_of_words.values())

        count_vectorizer = CountVectorizer()
        samples = [b['bag_of_words_features'] for b in bag_of_words]
        count_vectorizer._validate_vocabulary()
        bow_data = count_vectorizer.fit_transform(samples)
        normalized_bow_data = Normalizer().fit_transform(bow_data)
        normalized_bow_data = normalized_bow_data.A
        data_alg = np.append(data_alg, normalized_bow_data, axis = 1)
    
    #unique scaling for data
    #normalized_data_alg = StandardScaler().fit_transform(data_alg)

    draw_kmeans(data_alg, args)

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('k', help='Number of clusters', type=int)
    parser.add_argument('-g', '--genre', help="Genre", action="store_true")
    parser.add_argument('-s', '--style', help="Style", action="store_true")
    parser.add_argument('-y', '--year', help="Year", action="store_true")
    parser.add_argument('-v', '--version', help="Number of versions", action="store_true")
    args = parser.parse_args()
    if (not len(sys.argv) > 3 and (args.version or args.year)) or (len(sys.argv) <= 2):
        print('Not enough parameters, insert again!')
        sys.exit(0)
    else:
        main(args)
