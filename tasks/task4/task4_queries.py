
class Task4_queries:

    GET_RECORD_COUNT = """SELECT COUNT(*) FROM records"""

    GET_ALL_RECORDS_YEARS_VERSIONS = """SELECT year, num_version FROM records"""
    
    GET_ALL_RECORDS_IDS = """SELECT id FROM records"""

    GET_RECORDS_NUM_VERSIONS = """SELECT id, num_version FROM records"""

    GET_ALL_RECORDS_GENRES = """SELECT recordgenres.id_record, genres.name FROM
     recordgenres JOIN genres on recordgenres.id_genre = genres.id"""

    GET_ALL_RECORDS_STYLES = """SELECT recordstyles.id_record, styles.name FROM
     recordstyles JOIN styles on recordstyles.id_style = styles.id"""

