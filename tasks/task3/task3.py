import mysql.connector
from task3_queries import Task3_queries

import plotly
plotly.tools.set_credentials_file(username='Jeja', api_key='tgArmOZtUduUHXbaaKD9')

import plotly.plotly as py
import plotly.graph_objs as go

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="stampac",
    database="PSZ_project_DB"
    )

def do_task3():
    f = open("task3.txt", "w")
    f.write('3.a) ALBUMS PER DECADES:' + '\n\n')
    decade1 = write_decades(f, 1950, 1959)
    decade2 = write_decades(f, 1960, 1969)
    decade3 = write_decades(f, 1970, 1979)
    decade4 = write_decades(f, 1980, 1989)
    decade5 = write_decades(f, 1990, 1999)
    decade6 = write_decades(f, 2000, 2009)
    decade7 = write_decades(f, 2010, 2018)

    data_for_ploting = [go.Bar(
            x=['1950', '1960', '1970', '1980', '1990', '2000', '2010'],
            y=[decade1, decade2, decade3, decade4, decade5, decade6, decade7],
            name = 'Albums per decades'
    )]
    py.plot(data_for_ploting, filename='albums_per_decades', auto_open=True)

    f.write('\n')
    mycursor = mydb.cursor()
    mycursor.execute(Task3_queries.MOST_REPRESENTED_GENRES)
    data = mycursor.fetchall()
    f.write('3.b) 6 MOST REPRESENTED GENRES' + '\n\n')
    names = []
    values = []
    for d in data:
        f.write(str(d) + '\n')
        names.append(d[0])
        values.append(d[1])
    
    data_for_ploting = [go.Bar(
            x = names,
            y = values,
            name = '6 most represented genres'
    )]
    py.plot(data_for_ploting, filename='most represented genres', auto_open=True)  

    f.write('\n')
    f.write('3.c) SONG DURATION:' + '\n\n')

    arg1 = write_duration(f, 0, 90)
    arg2 = write_duration(f, 90, 180)
    arg3 = write_duration(f, 180, 240)
    arg4 = write_duration(f, 240, 300)
    arg5 = write_duration(f, 300, 360)

    data = (361,)
    mycursor.execute(Task3_queries.SONG_DURATION_MORE_360, data)
    data = mycursor.fetchall()
    f.write('more then 361 sec:' + '\n' + str(data[0][0]) + '\n')
    arg6 = data[0][0]
    data_for_ploting = [go.Bar(
            x=['< 90', '91-180', '181-240', '241-300', '301-360', '> 360'],
            y=[arg1, arg2, arg3, arg4, arg5, arg6],
            name = 'Tracks by duration'
    )]
    py.plot(data_for_ploting, filename='tracks_by_duration', auto_open=True)

    f.write('\n')
    f.write('3.d) CYRILIC AND LATINIC TITLES:' + '\n\n')
    mycursor.execute(Task3_queries.CYRILIC_TITLE_COUNT)
    data = mycursor.fetchall()
    cyrilic_num = data[0][0]
    mycursor.execute(Task3_queries.TOTAL_ALBUMS_COUNT)
    data = mycursor.fetchall()
    total_num = float(data[0][0])
    latinic_num = total_num - cyrilic_num
    f.write('cyrilic title number: ' + str(cyrilic_num) + '\n')
    f.write('latinic title number: ' + str(latinic_num) + '\n')
    f.write('percentage: ' + str(cyrilic_num/latinic_num*100) + '% \n')

    labels = ['Cyrilic','Latinic']
    values = [cyrilic_num,latinic_num]
    trace = go.Pie(labels=labels, values=values)
    py.plot([trace], filename='cyrilic_latinic')

    f.write('\n')
    f.write('3.e) NUM ALBUMS WITH SPECIFIC NUMBER OF GENRES: \n\n')

    arg1 = write_genres(f, 1, total_num)
    arg2 = write_genres(f, 2, total_num)
    arg3 = write_genres(f, 3, total_num)

    mycursor.execute(Task3_queries.ALBUMS_WITH_GENRE_MORE_4)
    data = mycursor.fetchall()
    f.write('4 and more genres: ' + str(data[0][0]) + '\n')
    arg4 = data[0][0]
    f.write('percentage:  \n ' + str(data[0][0]/total_num*100) + '%')
    
    labels = ['1 genre','2 genres', '3 genres', '4 genres and more']
    values = [arg1,arg2,arg3,arg4]
    trace = go.Pie(labels=labels, values=values)
    py.plot([trace], filename='genres')

    f.close()

def write_decades(f, dec_start, dec_end):
    mycursor = mydb.cursor()
    data = (dec_start, dec_end)
    mycursor.execute(Task3_queries.ALBUMS_DECADES, data)
    data = mycursor.fetchall()
    f.write(str(dec_start) + '-' + str(dec_end) + ':' + '\n' + str(data[0][0]) + '\n')
    return data[0][0]

def write_duration(f, start, end):
    mycursor = mydb.cursor()
    data = (start, end)
    mycursor.execute(Task3_queries.SONG_DURATION, data)
    data = mycursor.fetchall()
    f.write(str(start+1) + '-' + str(end) + 'sec:' + '\n' + str(data[0][0]) + '\n')
    return data[0][0]

def write_genres(f, value, total_num):
    mycursor = mydb.cursor()
    data=(value,)
    mycursor.execute(Task3_queries.ALBUMS_WITH_GENRE, data)
    data = mycursor.fetchall()
    f.write(str(value) + ' genre: ' + str(data[0][0]) + '\n')
    f.write('percentage:  \n' + str(data[0][0]/total_num*100) + '% \n')
    return data[0][0]

do_task3()
