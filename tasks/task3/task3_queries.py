#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Task3_queries:

    ALBUMS_DECADES="""SELECT COUNT(*) FROM records JOIN recordformats ON records.id=recordformats.id_record WHERE records.year >= (%s) AND records.year <= (%s) AND recordformats.format_name='Album' """

    MOST_REPRESENTED_GENRES = """SELECT genres.name, COUNT(*) FROM recordgenres
                                 JOIN genres ON genres.id=recordgenres.id_genre
                                 JOIN records ON records.id=recordgenres.id_record
                                 JOIN recordformats ON records.id=recordformats.id_record
                                 WHERE recordformats.format_name='Album'
                                 GROUP BY genres.name ORDER BY COUNT(*) DESC LIMIT 6"""

    SONG_DURATION = """SELECT COUNT(*) FROM tracks WHERE duration > (%s) AND duration <= (%s)"""

    SONG_DURATION_MORE_360 = """SELECT COUNT(*) FROM tracks WHERE duration > (%s)"""

    CYRILIC_TITLE_COUNT = """SELECT COUNT(*) FROM records JOIN recordformats ON records.id=recordformats.id_record WHERE records.title REGEXP '[Ѐ-ӿ]' AND recordformats.format_name='Album' """

    TOTAL_ALBUMS_COUNT = """SELECT COUNT(*) FROM records JOIN recordformats ON records.id=recordformats.id_record WHERE recordformats.format_name='Album'"""

    ALBUMS_WITH_GENRE = """SELECT COUNT(*) FROM records 
                               JOIN (SELECT * FROM recordgenres GROUP BY id_record HAVING COUNT(*)=(%s)) t
                               ON records.id=t.id_record
                               JOIN recordformats ON records.id=recordformats.id_record
                               WHERE recordformats.format_name='Album' """

    ALBUMS_WITH_GENRE_MORE_4 = """SELECT COUNT(*) FROM records 
                               JOIN (SELECT * FROM recordgenres GROUP BY id_record HAVING COUNT(*)>=4) t
                               ON records.id=t.id_record
                               JOIN recordformats ON records.id=recordformats.id_record
                               WHERE recordformats.format_name='Album' """

    