import mysql.connector
from queries_init_db import Queries_init_db

def make_first_connection():
    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="stampac"
    )
    return mydb

def make_DB():
    mydb = make_first_connection()
    mycursor = mydb.cursor()
    mycursor.execute("CREATE DATABASE IF NOT EXISTS PSZ_project_DB")
    create_tables()

def make_new_connection():
    mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="stampac",
    database="PSZ_project_DB"
    ) 
    return mydb

#create tables after you decide what exactly you need
def create_tables():
    mydb = make_new_connection()
    mycursor = mydb.cursor()
    mycursor.execute(Queries_init_db.CREATE_GENRES)
    mycursor = mydb.cursor()
    mycursor.execute(Queries_init_db.CREATE_STYLES)
    mycursor = mydb.cursor()
    mycursor.execute(Queries_init_db.CREATE_RECORDS)
    mycursor.execute(Queries_init_db.CREATE_RECORD_GENRES)
    mycursor.execute(Queries_init_db.CREATE_RECORD_STYLES)
    mycursor.execute(Queries_init_db.CREATE_RECORD_FORMATS)
    mycursor.execute(Queries_init_db.CREATE_RECORD_AUTHORS)
    mycursor.execute(Queries_init_db.CREATE_PERSONS)
    mycursor.execute(Queries_init_db.CREATE_TRACKS)
    mycursor.execute(Queries_init_db.CREATE_TRACK_ALBUMS) 

#change this somehow
make_DB()

    
