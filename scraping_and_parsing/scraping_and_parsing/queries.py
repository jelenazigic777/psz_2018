class Queries:

    INSERT_INTO_GENRES = """INSERT IGNORE INTO genres (name) VALUES (%s)"""

    INSERT_INTO_STYLES = """INSERT IGNORE INTO styles (name) VALUES (%s)"""

    INSERT_INTO_RECORDS = """INSERT INTO records (title, country, year, num_version) VALUES (%s, %s, %s, %s)"""

    INSERT_INTO_RECORD_GENRES = """INSERT INTO recordgenres (id_record, id_genre) VALUES (%s, %s)"""

    INSERT_INTO_RECORD_STYLES = """INSERT INTO recordstyles (id_record, id_style) VALUES (%s, %s)"""

    INSERT_INTO_RECORD_FORMATS = """INSERT INTO recordformats (id_record, format_name) VALUES (%s, %s)"""

    INSERT_INTO_RECORD_AUTHORS = """INSERT INTO recordauthors (id_record, author_name) VALUES (%s, %s)"""

    genre_id_query = """SELECT id FROM genres WHERE name=%s"""

    style_id_query = """SELECT id FROM styles WHERE name=%s"""

    INSERT_INTO_PERSONS = """INSERT IGNORE INTO persons (name, credits, vocals, wr_and_ar) VALUES (%s, %s, %s, %s)"""

    person_name_query = """SELECT id FROM persons WHERE name=%s"""

    check_if_track_exists = """SELECT id FROM tracks WHERE name=%s AND author=%s"""

    INSERT_INTO_TRACKS = """INSERT IGNORE INTO tracks (name, author, duration, num_albums) VALUES (%s,%s,%s,%s)"""

    get_num_albums = """SELECT num_albums FROM tracks WHERE name=%s AND author=%s"""

    update_num_albums = """UPDATE tracks SET num_albums=%s WHERE name=%s AND author=%s"""

    INSERT_INTO_TRACKALBUMS = """INSERT INTO trackalbums (id_album, id_track) VALUES (%s, %s)"""
