# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class Record(scrapy.Item):
    title = scrapy.Field()
    author = scrapy.Field()
    format = scrapy.Field()
    country = scrapy.Field()
    year = scrapy.Field()
    genre = scrapy.Field()
    style = scrapy.Field()
    num_version = scrapy.Field()

class Person(scrapy.Item):
    name = scrapy.Field()
    credits = scrapy.Field()
    vocals = scrapy.Field()
    wr_and_ar = scrapy.Field()

class Track(scrapy.Item):
    name = scrapy.Field()
    author = scrapy.Field()
    duration = scrapy.Field()
    num_albums = scrapy.Field()

