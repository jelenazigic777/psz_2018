import scrapy
from ..queries import Queries
from ..items import Record, Person, Track
from .getters import *
from start_urls import start_urls
import mysql.connector

from scrapy.http import Request,Response

class DiscogsSpider(scrapy.Spider):
    name = "discogs"

    def start_requests(self):
    
        urls = start_urls
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_all)

    def parse_all(self, response):

        #get all records on page
        entries = response.css('.card')
        for entry in entries:
            #get url, name and author
            url = get_page_url(entry)
            title = get_title(entry)
            #TODO: it can has more then one author!
            authors = get_authors(entry)
            authors_urls = get_authors_urls(entry)
            record = Record()
            record['title']=title
            record['author']=authors
            i=0
            for author in authors:
                person = Person()
                person['name'] = author
                author_url = authors_urls[i]
                if(author != 'Varius'):
                    request = Request(url = "https://www.discogs.com" + author_url, callback=self.parse_person)
                    request.meta['person'] = person
                    yield request
                i = i+1

            request = Request(url="https://www.discogs.com" + url, callback=self.parse_record)
            request.meta['record'] = record
            #now save it to table - genre, style and record
            yield request
        
        #TODO - CHECK THIS
        next_page = response.css('a[rel="next"]::attr(href)').extract_first()
        if next_page:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse_all)

    
    def parse_record(self, response):
        #here parse the record page
        record = response.meta['record']
        record['style']=get_style(response)
        record['genre']=get_genre(response)
        record['format']=get_format(response)
        record['country']=get_country(response)
        record['year']=get_year(response)
        record['num_version']=get_num_version(response)
        record_id = self.save_record_to_db(record)

        #parse and save songs
        self.parse_tracks(response, record['author'], record_id)
        return record
        
    def save_record_to_db(self, record):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="stampac",
        database="PSZ_project_DB"
        ) 
        #save record
        mycursor = mydb.cursor()
        record_data = (record.get('title'), record.get('country'), record.get('year'), record.get('num_version'))
        mycursor.execute(Queries.INSERT_INTO_RECORDS, record_data)
        mydb.commit()

        #get the id of inserted record
        record_id = mycursor.lastrowid

        #save authors
        for author in record.get('author'):
            author_data = (record_id, author)
            mycursor.execute(Queries.INSERT_INTO_RECORD_AUTHORS, author_data)
            mydb.commit()

        #save genre and style
        if record.get('genre'):
            for genre in record.get('genre'):
                genre_data = (genre,)
                mycursor.execute(Queries.INSERT_INTO_GENRES, genre_data)
                mydb.commit()

                #get id of genre
                mycursor = mydb.cursor()
                mycursor.execute(Queries.genre_id_query,genre_data)
                genre_id = mycursor.fetchone()[0]

                genre_record_data = (record_id, genre_id)
                mycursor.execute(Queries.INSERT_INTO_RECORD_GENRES, genre_record_data)
                mydb.commit()
        
        if record.get('style'):
            for style in record.get('style'):
                style_data = (style,)
                mycursor.execute(Queries.INSERT_INTO_STYLES, style_data)
                mydb.commit()

                #get id of style
                mycursor = mydb.cursor()
                mycursor.execute(Queries.style_id_query,style_data)
                style_id = mycursor.fetchone()[0]

                style_record_data = (record_id, style_id)
                mycursor.execute(Queries.INSERT_INTO_RECORD_STYLES, style_record_data)
                mydb.commit()
        
        #save format
        if record.get('format'):
            for formatt in record.get('format'):
                format_record_data = (record_id, formatt)
                mycursor.execute(Queries.INSERT_INTO_RECORD_FORMATS, format_record_data)
                mydb.commit()
        
        return record_id
    

    def parse_person(self, response):
        person = response.meta['person']
        person['credits'] = get_credits(response)
        person['vocals'] = get_vocals(response)
        person['wr_and_ar'] = get_wrar(response)
        self.save_person_to_db(person)
        return person
    
    def save_person_to_db(self, person):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="stampac",
        database="PSZ_project_DB"
        ) 
        #save person

        #check if it is already in db
        mycursor = mydb.cursor()

        person_name=(person.get('name'),)
        mycursor.execute(Queries.person_name_query, person_name)
        person_ids = mycursor.fetchone()
        if not person_ids or len(person_ids) == 0:
            person_data = (person.get('name'), person.get('credits'), person.get('vocals'), person.get('wr_and_ar'))
            mycursor.execute(Queries.INSERT_INTO_PERSONS, person_data)
            mydb.commit()

    def parse_tracks(self, response, authors, record_id):
        #get tracks and save them to db
        tracks = get_tracklist(response)
    
        for t in tracks:
            track = Track()
            track['author'] = authors[0]
            track['name'] = get_track_name(t)
            track['duration'] = get_track_duration(t)
            #save tracks to db
            self.save_track_to_db(track, record_id)

    def save_track_to_db(self, track, record_id):
        mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="stampac",
        database="PSZ_project_DB"
        )
        #first check if track already exist
        mycursor = mydb.cursor()
        data = (track.get('name'), track.get('author'))
        mycursor.execute(Queries.check_if_track_exists, data)
        track_id = 0
        track_ids = mycursor.fetchone()
        mydb.commit()

        mycursor = mydb.cursor()
        #if already exists - just update num_albums
        if track_ids:
            track_id = track_ids[0]
            mycursor.execute(Queries.get_num_albums, data)
            num_albums = mycursor.fetchone()[0]

            mycursor = mydb.cursor()
            new_data = (num_albums+1,track.get('name'), track.get('author'))
            mycursor.execute(Queries.update_num_albums, new_data)
            mydb.commit()
        else:
            #in this case - add to db and set num_albums to 1
            new_data = (track.get('name'), track.get('author'), track.get('duration'), 1)
            mycursor.execute(Queries.INSERT_INTO_TRACKS, new_data)
            track_id = mycursor.lastrowid
            mydb.commit()
        #now add id of album to the table
        mycursor = mydb.cursor()
        new_data = (record_id, track_id)
        mycursor.execute(Queries.INSERT_INTO_TRACKALBUMS, new_data)
        mydb.commit()

        


        

