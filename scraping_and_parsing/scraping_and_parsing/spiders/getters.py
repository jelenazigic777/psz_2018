import re

#TODO: it can be more then one author!!!
def get_authors(response):
    return response.css('h5 > span::attr(title)').extract()

def get_page_url(response):
    return response.css('h4 > a::attr(href)').extract_first()

#TODO: it can be more then one author!!!
def get_authors_urls(response):
    return response.css('h5 > span > a::attr(href)').extract()

def get_title(response):
    return response.css('h4 > a::attr(title)').extract_first()

def get_format(response):
    all_contents = response.css('div.profile div.content').extract()
    formats = ''
    for i in all_contents:
        if 'format' in i:
            formats = i
    formats = ''.join(formats.split())
    all_formats = formats.replace("<br>","").replace("</div>","").replace('</a>"',',').replace('<i>',',').split(',')
    final_formats = []
    for f in all_formats:
        if ('format_exact=') in f:
            help_str=f[f.find('format_exact=')+13:len(f)].replace('</a>','').replace('">', '')
            final_formats.append(help_str[0:len(help_str)/2])
        elif('</i>') in f:
            final_formats.append(f[0:len(f)-4])
        else:
            if f: final_formats.append(f)
    return final_formats
    
def get_country(response):
    links = response.css('div.profile div.content a::attr(href)').extract()
    for link in links:
        if "country" in link:
            return link.split('country=')[1]
    return ''

#more cases here - 0 if not set
def get_year(response):
    returned_value = response.css('a[href*="year="]::text').extract_first()
    if not returned_value:
        return 0
    returned_value = returned_value.rstrip().lstrip()
    if len(returned_value) > 4:
        returned_value = returned_value[len(returned_value)-4:len(returned_value)]
    return returned_value

def get_genre(response):
    return response.css('a[href^="/genre"]::text').extract()

def get_style(response):
    return response.css('a[href^="/style"]::text').extract()

def get_num_version(response):
    response = response.css('#m_versions > h3::text').extract_first()
    if not response:
        return 1
    list_of_nums = map(int, re.findall('\d+', response))
    return list_of_nums.pop()

def get_credits(response):
    value = response.css('div#discography_wrapper ul:nth-child(4) > li:nth-child(1) > h3 > a > span::text').extract_first()
    if value: 
        return value
    else: 
        return 0

def get_vocals(response):
    value = response.css('div#discography_wrapper ul:nth-child(4) > li > a').extract_first()
    if value:
        value = value.split('</a>')
        for v in value:
            if ('data-credit-subtype="Vocals"') in v:
                v = ''.join(v.split())
                new_value = v[v.find('facet_count')+13:v.find('</span>Vocals')]
                return new_value               
    return 0

def get_wrar(response):
    values = response.css('div#discography_wrapper ul[class=facets_nav] > li > a').extract()
    for value in values:
        if value:
            if ('data-credit-subtype="Writing-Arrangement"') in value:
                value = ''.join(value.split())
                new_value = value[value.find('facet_count')+13:value.find('</span>Writing&amp;Arrangement')]
                return new_value               
    return 0

def get_tracklist(response):
    tracklist = response.css('table.playlist')
    return tracklist.css('tr')

def get_track_name(track):
    return track.css('td.tracklist_track_title span::text').extract_first()

def get_track_duration(track):
    duration_string = track.css('td.tracklist_track_duration span::text').extract_first()
    if duration_string:
        minutes = duration_string.split(':')[0]
        sec = duration_string.split(':')[1]
        return int(minutes) * 60 + int(sec)
    else: 
        return 0
    




